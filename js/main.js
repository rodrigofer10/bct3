$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
         interval: 2000
    });
    $('#modalCompra').on('show.bs.modal', function (e){
        console.log('El modal de asignación de cantidades se está mostrando');

        $('.btn-reserva').removeClass('btn-primary');
        $('.btn-reserva').addClass('btn-outline-primary');
        $('.btn-reserva').prop('disabled', true);
    });
    $('#modalCompra').on('shown.bs.modal', function (e){
        console.log('El modal de asignación de cantidades se mostró');
    });
    $('#modalCompra').on('hide.bs.modal', function (e){
        console.log('El modal de asignación de cantidades se está ocultando');
    });
    $('#modalCompra').on('hidden.bs.modal', function (e){
        console.log('El modal de asignación de cantidades se ocultó');

        $('.btn-reserva').removeClass('btn-outline-primary');
        $('.btn-reserva').addClass('btn-primary');
        $('.btn-reserva').prop('disabled', false);
    });
});